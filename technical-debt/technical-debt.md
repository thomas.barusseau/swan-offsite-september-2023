---
marp: true
math: mathjax
---
<!-- class: invert -->

![bg left invert height:100px](assets/logo.png)

## Managing technical debt
Swan offsite - **September 2023**

---
<!-- _header: "Introduction | Table of content" -->
<!-- footer: "Swan offsite - **September 2023**" -->
<!-- paginate: true -->

![bg blur:3px right:33%](assets/drunk-swan.jpg)

# Table of content

1. Technical debt
    a. What is it?
2. Debt management history at Swan
    a. What worked, what didn't
    b. What's new with the squads organisation?
3. The future
    a. Handling existing debt
    b. Preventing debt
    c. The things we shall not talk about

---
<!-- header: "Technical debt | What is it?" -->

![bg blur:3px left:33%](assets/swan-beach.jpg)

# Technical debt
## What is it?

---
> Technical debt is the implied cost of future reworking required when choosing an easy but limited solution instead of a better approach that could take more time.

Source: Wikipedia

---
<!-- _header: "" -->
<!-- _footer: "" -->

![bg height:100%](assets/intentional-unintentional.webp)

---
# Technical debt visualized

---
<!-- _header: "" -->
<!-- _footer: "" -->

![bg height:100%](assets/technical-debt-4.png)

---
<!-- _header: "" -->
<!-- _footer: "" -->

![bg height:100%](assets/technical-debt-5.png)

---
<!-- header: "Debt management history at Swan | What worked, what didn't?" -->

![bg blur:3px right:33% width:1200](assets/bg-2.jpg)

# Debt management history at Swan
## What worked, what didn't?

---
At the beginning:
- No debt, focus on design 👶
- Started growing, shape-up with 1 week/month 🌨️
- ️Deal with tech improvements continuously ⚙️

---
Current issues:
- Lack of ownership 🐦
- Lack of vision 👁️
- Hard subjects are ignored 🙈
- Relies on personal initiative 🏃

---
<!-- header: "Debt management history at Swan | What's new with the squads organisation?" -->

![bg blur:3px left:33% width:1200](assets/bg-3.jpg)

# Debt management history at Swan
## What's new with the squads organisation?

---
The good:
- Smaller teams, more ownership
- Narrow scope
- More autonomy

---
The bad:
- Teams are more spread
- Harder to tackle big projects

---
<!-- header: "The future | Handling existing debt" -->
![bg blur:3px right:33% width:1200](assets/bg-4.jpg)

# The future
## Handling existing debt

---
To deal with existing debt:

- Measure
- Prioritize
- Ongoing process
- Move mountains

---
<!-- header: "The future | Preventing debt" -->
![bg blur:3px left:33% width:1200](assets/bg-5.jpg)

# The future
## Preventing debt

---
<style>
img[alt~="center"] {
  display: block;
  margin: 0 auto;
}
</style>

![center](assets/gandalf.jpg)

Source: Gandalf... Surely?

---
The best way to fight technical debt is to not accumulate it in the first place.

How do we do that?

---
![bg right width:90%](assets/prevent-1.jpg)
1. Be conscious about technical debt.


---
![bg right width:90%](assets/prevent-2.png)
2. Start talking about technical debt.

---
![bg right width:90%](assets/prevent-3.jpg)
3. Don't sweep technical debt under the rug.

---
![bg right height:90%](assets/prevent-4.png)
4. Don't be affraid to refactor code (i.e.: have a ton of tests).

---
![bg right width:90%](assets/prevent-5.png)
5. Review the code.

---
![bg right width:90%](assets/prevent-6.jpg)
6. Don't rush the process!

---
<!-- header: "The future | The t̸h̵i̸n̵g̴s we shall n̶o̸t̸ ̶t̸a̶l̴k̸ ̵a̸b̴o̷u̷t̵" -->
![bg blur:5px right:33% width:1200](assets/chtulu.jpg)

# The future
## The t̸h̵i̸n̵g̴s we shall n̶o̸t̸ ̶t̸a̶l̴k̸ ̵a̸b̴o̷u̷t̵
###### (i.e. the other sources of technical debt)

---
### Abandoned merge requests

Currently opened MR in some repositories: (including bot MRs)
- `back-office-api`: 28
- `account-contract-api`: 20
- `projects` (`iam`, `federation`...): 22
- `payment-api`: 67

---
### Abandoned merge requests

- "La flemme 🇫🇷" to do an analysis, but mind the size of your MRs.
- Take time to review new MRs! « One must strike while the iron is hot. »

---
Sources of technical debt in **documentation**:

- Not enough of it.
- Outdated documentation.
- Too much of it.
- Inaccurate documentation.

---
<!-- _header: "" -->
<!-- _footer: "" -->
<!-- _paginate: false -->
![bg height:100%](assets/superhero.png)

---
Sources of technical debt in **Notion**:

- Unused pages.
- Outdated pages.
- Loss of self-discoverability.

---
Sources of technical debt in **Linear**:

- The unused statuses.
- The forgotten tickets.
- The worthless priority.
- The abandoned projects.

---
Sources of technical debt in **other things**:

- Processes
- Bot integrations
- Multiplication of tools
- Invisible application flows/dependencies

---
<!-- header: "" -->
<!-- footer: "" -->
<!-- paginate: false -->

![bg blur:5px right](assets/ending-1.jpg)

# Let's talk about it!

---
# Thank you for coming to my TED talk. Now you do the talking. Cheers 🍻
