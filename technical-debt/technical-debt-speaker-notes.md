# Swan debt management history

At the beginning:

- Not really any debt management, focused on moving forward. Debt was not a big issue because early on we worked on high quality design 👶
- We started growing, following the "shape-up" methodology with 1 week every month of "cooldown", focusing on tech improvements 🌨️
- Then, we decided that we actually wanted to deal with tech improvements continuously. Product managers know the importance of tech improvements and we try to keep 25% of our work on it ⚙️

Current issues:

- Lack of ownership because responsibility is split (`iam`, `account-contract` repositories for example).
- Lack of overall vision because this subject isn't worked on at a higher level.
- Some subjects are complex and long to treat, and thus have low priority (and are never done).
- This way of doing things relies on personal initiative.

# Dealing with existing debt

How can we efficiently deal with existing debt?

- Know what we're talking about: measure the debt, if possible with analytical data.
- Prioritize tech improvement tickets instead of always setting them to "low".
- Recognize processing technical debt is an ongoing process.
- Tackle the **big** and **important** refactorings, even if it means pushing release dates.

# Preventing debt

1. The simple fact of being aware of technical debt is enough to prevent some of it, as most Swanees are now careful about it.
2. The more, the better. Adding a biweekly or monthly discussion regarding any kind of debt (be it grooming, tech discovery) is probaly a good idea.
3. Every instance of technical debt that's correctly identified should be assigned a ticket (which should not be immediatly forgotten after creation).
4. Getting rid of technical debt usually implies some refactoring, at some point. You want to be able to refactor everything without breaking anything.
5. This is similar to avoiding bugs: if more people review a piece of code, there is less chance that a bug makes it through the review process. This also works with technical debt, as points can be discussed if they might create additional debt.
6. The biggest reason for technical debt in the first place is when technical teams have tight deadlines to meet. Technical debt is a direct result of cutting corners (when the corners are code quality).

# The things we shall not talk about

## Abandoned MR

- The bigger it is, the longer it takes to be fully reviewed, and the more modifications there might need to do.
- The longer a MR exists, the less chances it has to ever be merged.
- Take care of the MR of your teammates!

## Documentation

- That's the issue with documentation: the more you write, the more you must maintain (usually no one maintains documentation).
- If you can prevent writing documentation, that's better (self-documenting code). But usually complexe processes need some sort of documentation, or else you end up with...

## Documentation: superhero syndrome

- When one person holds too much information.
- It is dangerous in the long run.

## Notion pages

- Unused pages: fills the Notion with a lot of useless stuff.
- Outdated pages: fills the Notion with a lot of misleading stuff.
- Loss of self-discoverability: Notion search only works through titles, as such we must get rid of unused/outdated pages and use the correct page titles.

## Linear

- Usually a problem linked to day-to-day operations/processes, but must be dealt with else the tool becomes useless.

## Other things

- Processes that can paralyze teams and destroy velocity.
- Bots/app integrations that aren't that useful but create some mental overhead/context switching.
- Spreading the information across multiple tools, making information search complicated and things slower overall.
- Can slow down everything because it creates hidden dependencies between parts of the product that don't appear to have a direct dependency.
