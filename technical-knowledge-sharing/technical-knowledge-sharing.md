---
marp: true
math: mathjax
---

<!-- Having a clear process to allow Swanee developers to share technical knowledge to others (which might be completely unrelated to what we use at Swan) helps technical growth of the teams, which can help shape the future technical decisions in teams.

Example of things we can discuss: coding kata/dojo exploring methodologies/technologies, allocating slots for technical presentations about things that some people want to share with each other, offering the opportunity to discuss new, shiny technologies and how they could integrate into Swan/what we could gain from them.

Also to be explored: superhero problem.

Les Tech Chapter: une heure toutes les deux semaines pour parler de sujet tech, animé par Alix et moi. Les sujets étaient à l'initiative des gens. Puis ça s'est tari parce que manque de sujets, et Alix et moi devenions vraiment occupés
Les Conf internes: format meetup, il n'y a eu que deux éditions, personne pour maintenir l'initiative, trop peu de contributions. On a stoppé naturellement
Globalement, on manque d'ownership sur ces sujets. On imagine avec Mat et Mika que ce sera la responsabilité prochaine des staff engineer. Rien n'a été mis en place encore. Q4 j'espère  -->

<!-- class: invert -->

![bg left invert height:100px](assets/logo.png)

## Technical knowledge sharing
Swan offsite - **September 2023**

---
<!-- _header: "Introduction | Table of content" -->
<!-- footer: "Swan offsite - **September 2023**" -->
<!-- paginate: true -->

![bg blur:3px right:33%](assets/drunk-swan.jpg)

# Table of content

1. Technical knowledge sharing
    a. What is it?
    b. Problems with lack of knowledge sharing
2. Knowledge sharing at Swan
    a. What we're no longer doing
    b. What we're still doing
3. Sharing is caring
    a. (Re-)Establishing processes
    b. How to get started

---
<!-- header: "Technical knowledge sharing | What is it?" -->

![bg blur:3px height:120% left:33%](assets/bg-1.jpg)

# Technical knowledge sharing
## What is it?

---
# Knowledge sharing

> Knowledge sharing is an activity through which knowledge (namely, information, skills, or expertise) is exchanged [...] within organizations.

Source: Wikipedia

---
# Why share the knowledge?

- Improves the organizational knowledge
- Leads to competitive advantage
- Bridges the individuals

---
# Explicit and tacit knowledge sharing
## Explicit knowledge

- Documentation
- Manuals
- Knowledge center

---
# Explicit and tacit knowledge sharing
## Tacit knowledge

- Experience
- Insight
- Intuition

---
<!-- header: "Technical knowledge sharing | Lack of it" -->

![bg blur:3px height:120% right:33%](assets/bg-2.jpg)

# Technical knowledge sharing
## Problems with lack of knowledge sharing

---
# Lack of growth within teams

Multiple ways towards the technical enlightening:

- Individual growth
- Shared growth

---
# Lack of growth within teams

- Individuals usually don't commit to individual growth
- People don't have the opportunity to share
- Result: people stagnate

---
# Superhero syndrome

- One person carries too much on their shoulders
- One person is the only owner for a specific part of the product/infrastructure
- One person is too effective

---
# Superhero syndrome

>In the realm of code, a hero's light did gleam,
Shouldering burdens, chasing a distant dream,
But the "Superhero syndrome" took its toll,
In burnt-out embers, he left a saddened soul.

>Farewell to the dev, who carried worlds on his back,
Now rests in peace, from that relentless track.
Let us remember his lessons learned too late,
To balance our powers before it's too great.

---
<!-- header: "Knowledge sharing at Swan | The past" -->

![bg blur:3px height:120% left:33%](assets/bg-3.jpg)

# Knowledge sharing at Swan
## The past

---
# Tech chapters
- One hour every 2 weeks
- Any tech subject
- Animated by Jeremy and Alix
- Lack of subjects and time 🪦

---
# Internal conferences
- Meetup format
- Tried two times
- Not enough contributions
- It stopped naturally 🪦

---
<!-- header: "Knowledge sharing at Swan | The present" -->

![bg blur:3px height:120% right:33%](assets/bg-4.jpg)

# Knowledge sharing at Swan
## The present

---
# Tech onboarding

- Get new Swanees up to speed
- Introduce tools, frameworks, environment
- Try to develop autonomy as much as possible

---
# Pair programming/review

- Take someone else's hand to develop a feature or review a MR
- Especially effective with newcomers/juniors/people who know nothing about a feature

---
<!-- header: "Sharing is caring | Other existing processes" -->

![bg blur:3px height:120% left:33%](assets/bg-5.jpg)

# Sharing is caring
## (Re-)Establishing processes

---
# Technical presentations (i.e. tech chapters)

- Still based on volunteering
- Leave the slot up whether it's used or not
- Incentivize people to participate
- Can be Swan-related or not

---
# Round tables

- Aggregate subjects in a notion table
- When we have enough matters to discuss, schedule a 1-hour meeting
- People share their discoveries/experimentations and if it sparks discussion, cool, if it doesn't, not a big deal
- Anyone can participate

---
# Coding dojos

- Hone your mad coding $killz
- Explore new methodologies (pair prog pilot/navigator, mob prog)
- Learn new technologies/languages/frameworks (Scala? Everyone wants to work in team Payment)
- Discuss whether or not these kind of things would be relevant within Swan teams

---
<!-- header: "Sharing is caring | How to get started" -->

![bg blur:3px height:120% right:33%](assets/bg-6.jpg)

# Sharing is caring
## How to get started

---
<!-- _header: "" -->
<!-- _footer: "" -->
<!-- _paginate: false -->

![bg height:90%](assets/just-do-it.gif)

---
# Roadmap
- Discuss the subject (it's today)
- Document the preferred processes
- Book recurring slots
- Give real-life money to the first 10 people participating (no sandbox bullshit)

---
<!-- header: "" -->
<!-- footer: "" -->
<!-- paginate: false -->

![bg blur:5px right](assets/ending-1.jpg)

# Let's talk about it!

---
# Thank you for coming to my TED talk. Now you do the talking. Cheers 🍻

