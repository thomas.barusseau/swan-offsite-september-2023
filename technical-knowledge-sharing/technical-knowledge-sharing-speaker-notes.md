# Technical knowledge sharing
## What is it?

### Why share the knowledge

### Explicit knowledge
Explicit knowledge is the kind that can easily be recorded and accessed.

### Tacit knowledge
Tacit knowledge builds the context around explicit knowledge.
Understand the past decisions, why things work this way, what something might impact.

This is much more difficult to share tacit knowledge, even though it's probably the most important kind of knowledge in the long run.

## Problems with lack of knowledge sharing
### Lack of growth within teams

Individual growth: books, online resources, hobby experimentations/PoC. It takes time and energy, usually outside of the working hours.

Shared growth: courses with a tutor, sharing discoveries/experimentations, talking about new technologies. This usually isn't done except if allowed within a company because of the time it takes to prepare.

Next slide:
- Most people won't commit time and energy to individual growth.
- If people don't have time within a company to share their knowledge, they won't be able to do so.
- If people stagnate in term of technical level, in reality they regress because of how fast the tech world moves.

### Superhero syndrome

Read the bullet list.

Having a superhero in your company is great, until...
- They no longer have the bandwidth to handle all the subjects they juggle with
- They get sick or worse, burn out (it could happen to you)
- They leave the company

Here is a poem about it that I definitely wrote on my own.
-> NEXT SLIDE

# Knowledge sharing at Swan
## The past
### Tech chapters
Read the list

### Internal conferences
Read the list

## The present
### Tech onboarding
This initiative is quite recent and still a work in progress, but I feel like it's a step in the right direction.

### Pair programming/review
People are still shy in regard to pair programming even though, if properly used, it can be a very efficient growth and productivity tool.
This is mostly about educating the devs so that they have the habit of pair programming.

# Sharing is caring
## (Re-)Establishing processes
### Technical presentations
Read the list
How to incentivize? For starters, leave some time for people to prepare presentation materials during office hours. It's going to take 2-3 hours for one person so that 10+ persons can learn something from it. It's worth it if the subject is interesting to a group of other people!
By the way, these presentations don't have to be technical, especially if we struggle to find volunteers. Just propose subjects in a notion table, have a dedicated slack channel to promote your stuff, and see if there is interest or not. Then schedule at a specific date and roule ma poule.
In regard to Swan-specific subjects: a superhero can use this opportunity to do a deep dive in a subject.

### Round tables
-

### Coding dojos
-

## How to get started
It's simple: next slide
